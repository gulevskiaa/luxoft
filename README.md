To compile code and run the app, open project in Android Studio, plug device or emulator, then click "Run/Run app".

#### MainActivity

Main screen of the app with demonstration it's functionality.<br/>
Layout - activity_main with ConstraintLayout as a root.<br/>

#### Robber

##### RobberCalculator

Contains function getMaxAmountOfMoney that calculates maximum sum of non-adjacent numbers in array.<br/>
Assumptions:
1. Sum of money in houses must be less or equal than Int.MAX_VALUE (2 147 483 647)<br/>
2. Count of houses must be less or equal than Int.MAX_VALUE (2 147 483 647)<br/>

Time complexity of algorithm is O(n) because in this case we need to go through all elements in array.<br/>
Space complexity - 0(1)

##### RobberCalculatorTest

Unit test for RobberCalculator

##### InputDataValidator

Was added for making some simple validation for input strings

##### InputDataValidatorTest

Unit test for InputDataValidator

##### function hideKeyboard in KeyboardUtils.Kt

To hide soft keyboard

#### Employees

##### model classes (Address, Employee, Report)

simple data classes containing data related to the objects

##### package Repository (interface and implementation)

Encapsulates logic for working with Data layer.<br/>
In our case simply persists data in the memory (HashMap with employees).<br/>
In production should be injected to Presenter/Viewmodel.<br/>
Can be covered by tests simply (ex. EmployeeRepositoryImplTest).<br/>
I added another HashMap with names for better performance while searching by names.<br/>
For simplicity id increments by 1 for every new employee.