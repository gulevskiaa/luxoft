package ru.gulevski.luxoft.organization.repository

import ru.gulevski.luxoft.organization.model.Employee
import ru.gulevski.luxoft.organization.exception.RepositoryException
import ru.gulevski.luxoft.organization.model.Address
import ru.gulevski.luxoft.organization.model.Report

/**
 * Concrete realization of EmployeeRepository
 * In production app it'd encapsulate logic for working with Database
 *
 * Assume that id, name and parentId we can't change.
 * id - is a primary key and assume that it increments for every new added employee
 * To change:
 *  - "name" we should implement additional method like changeName etc.
 *  - "parentId" we should implement additional methods like moveEmployee etc.
 *
 *  !!! Not thread safe class
 */
internal class EmployeeRepositoryImpl : EmployeeRepository {
    private val employees = hashMapOf<Int, Employee>()

    /**
     * Storing for names-id's mapping (for query by name, analog indexable field "name" in Database)
     */
    private val nameIdMap = hashMapOf<String, HashSet<Int>>()
    private var lastId = 0

    companion object {
        private const val MESSAGE_NAME_CANT_BE_BLANK = "Name can't be blank"
    }

    /**
     * Assumption: returning the first one with such a name
     */
    override fun getEmployeeByName(name: String): Employee? {
        if (name.isBlank()) throw RepositoryException(MESSAGE_NAME_CANT_BE_BLANK)

        val ids = nameIdMap[name.toLowerCase()]
        if (ids.isNullOrEmpty()) return null

        val employee = employees[ids.first()]

        // Get and set parent name
        employee?.let {
            it.parentName = employees[it.parentId]?.parentName
        }
        return employee
    }

    override fun addEmployee(
        name: String,
        age: Int,
        address: Address,
        currentReports: List<Report>,
        parentId: Int?
    ) {
        if (name.isBlank()) throw RepositoryException(MESSAGE_NAME_CANT_BE_BLANK)

        val tempId = lastId + 1
        val employee = Employee(tempId, name, age, address, currentReports, parentId)

        checkParentExistence(employee)

        employees[employee.id] = employee
        lastId = tempId

        addNameForSearching(employee)
    }

    private fun checkParentExistence(employee: Employee) {
        val parent = employees[employee.parentId]
        if (parent == null) {
            val incorrectIdMessage = "There is not parent with such an ID"

            if (employees.isEmpty()) {
                if (employee.parentId != null) {
                    throw RepositoryException(incorrectIdMessage)
                }
            } else {
                if (employee.parentId == null) {
                    throw RepositoryException("There couldn't be more than one root parent")
                }

                throw RepositoryException(incorrectIdMessage)
            }
        }
    }

    private fun addNameForSearching(employee: Employee) {
        val idsByName = nameIdMap[employee.name]
        if (idsByName == null) {
            nameIdMap[employee.name.toLowerCase()] = hashSetOf(employee.id)
        } else {
            idsByName.add(employee.id)
        }
    }

    override fun removeAll() {
        employees.clear()
        nameIdMap.clear()
        lastId = 0
    }
}