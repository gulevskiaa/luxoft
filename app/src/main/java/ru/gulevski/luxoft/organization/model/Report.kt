package ru.gulevski.luxoft.organization.model

internal data class Report(
    val title: String,
    val contents: String
)