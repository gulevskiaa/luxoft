package ru.gulevski.luxoft.organization.model

/**
 * Describes employee
 */
internal data class Employee(
    val id: Int,
    val name: String,
    var age: Int,
    var address: Address,
    var currentReports: List<Report>,
    val parentId: Int?,
    var parentName: String? = null
) {
    override fun equals(other: Any?): Boolean {
        if (other == null) return false

        if (this::class != other::class) return false

        return id == (other as Employee).id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "Employee(id=$id, name=$name)"
    }
}