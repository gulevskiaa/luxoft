package ru.gulevski.luxoft.organization.model

internal data class Address(
    val postcode: String? = null,
    val country: String? = "US",
    val region: String? = null,
    val city: String? = null,
    val street: String? = null,
    val house: String? = null,
    val flat: Int? = null
)