package ru.gulevski.luxoft.organization.exception

import java.lang.RuntimeException

/**
 * Presents exceptions thrown while working with Employers on Data Layer
 */
internal class RepositoryException(message: String): RuntimeException(message)