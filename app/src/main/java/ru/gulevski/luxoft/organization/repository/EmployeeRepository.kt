package ru.gulevski.luxoft.organization.repository

import ru.gulevski.luxoft.organization.model.Address
import ru.gulevski.luxoft.organization.model.Employee
import ru.gulevski.luxoft.organization.model.Report

/**
 * Encapsulate logic for working with data-layer
 */
internal interface EmployeeRepository {

    /**
     * Get employee by name
     * @param name - name of Employee
     * @return Employee
     */
    fun getEmployeeByName(name: String): Employee?

    /**
     * Add employee
     * @param name - Employee's name
     * @param age - Employee's age
     * @param address - Employee's address
     * @param currentReports - reports
     * @param parentId - id of parent Employee
     */
    fun addEmployee(
        name: String,
        age: Int,
        address: Address,
        currentReports: List<Report>,
        parentId: Int?
    )

    /**
     * Removes all employers
     */
    fun removeAll()
}