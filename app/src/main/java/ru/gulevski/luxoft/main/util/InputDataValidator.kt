package ru.gulevski.luxoft.main.util

import java.lang.NumberFormatException
import java.lang.RuntimeException

/**
 * Makes data validation
 */
internal class InputDataValidator {

    /**
     * Validate data
     * @param input - string
     * @return array of ints
     * @throws ValidatorException if data can't  be parsed to Int Array
     * Assumption:
     * Values cannot be bigger than Int.MAX_VALUE (2 147 483 647)
     */
    fun validateIntArray(input: String): IntArray {
        try {
            val values = input.replace(" ", "").split(",").map { it.toInt() }.toIntArray()
            values.sum()
            return values
        } catch (ex: NumberFormatException) {
            throw ValidatorException(
                "Input data is not valid.\n" +
                        "Use non-negative numbers (not bigger than 2 147 483 647) separated by commas. \n "
            )
        }
    }

    /**
     * Validate data
     * @param input - string
     * @return int
     * @throws ValidatorException if data can't  be parsed to Int
     * Assumption:
     * Values cannot be bigger than Int.MAX_VALUE (2 147 483 647)
     */
    fun validateIntValue(input: String): Int {
        try {
            return input.toInt()
        } catch (ex: NumberFormatException) {
            throw ValidatorException("Input data is not valid.")
        }
    }

    /**
     * Validate data
     * @param input - string
     * @return int or null if string is empty
     */
    fun validateParentId(input: String): Int? {
        if (input.isEmpty()) return null
        return validateIntValue(input)
    }

    internal class ValidatorException(message: String) : RuntimeException(message)
}