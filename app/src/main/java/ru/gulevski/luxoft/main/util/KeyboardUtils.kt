package ru.gulevski.luxoft.main.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

internal fun hideKeyboard(view: View) {
    val imManager = view.context
        .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imManager.hideSoftInputFromWindow(view.windowToken, 0)
}