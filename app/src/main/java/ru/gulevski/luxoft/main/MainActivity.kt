package ru.gulevski.luxoft.main

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import ru.gulevski.luxoft.R
import ru.gulevski.luxoft.main.util.hideKeyboard
import ru.gulevski.luxoft.organization.repository.EmployeeRepository
import ru.gulevski.luxoft.organization.repository.EmployeeRepositoryImpl
import ru.gulevski.luxoft.main.util.InputDataValidator
import ru.gulevski.luxoft.organization.exception.RepositoryException
import ru.gulevski.luxoft.organization.model.Address
import ru.gulevski.luxoft.robber.RobberCalculator

class MainActivity : AppCompatActivity() {

    /**
     *  In production app fields below should be injected with some DI framework (for ex. Dagger2) and moved to some
     *  class with presentation logic (Presenter, ViewModel.
     *  For simplicity placed them here
     *  employeeRepository should have one instance during lifecycle of the App.
     *
     *  In production app all operations with data must do in background to not freeze UI
     */
    private val employeeRepository: EmployeeRepository = EmployeeRepositoryImpl()
    private val validator = InputDataValidator()
    private val robberCalculator = RobberCalculator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRobberBlock()
        initEmployeesBlock()
    }

    private fun initRobberBlock() {
        val housesEditText = findViewById<EditText>(R.id.housesEditText)
        val calcSumButton = findViewById<Button>(R.id.calcSumButton)

        calcSumButton.setOnClickListener {
            val intArray: IntArray
            try {
                intArray = validator.validateIntArray(housesEditText.text.toString())
                showToast(robberCalculator.getMaxAmountOfMoney(intArray).toString())
            } catch (ex: InputDataValidator.ValidatorException) {
                showError(ex.localizedMessage ?: "")
            }
            hideKeyboard(this.rootLayout)
        }
    }

    private fun initEmployeesBlock() {
        addEmployeeBlock()
        getEmployeeBlock()
    }

    private fun addEmployeeBlock() {
        val employeeNameEditText = findViewById<EditText>(R.id.employeeNameEditText)
        val employeeParentIdEditText = findViewById<EditText>(R.id.employeeParentIdEditText)
        val employeeAgeEditText = findViewById<EditText>(R.id.employeeAgeEditText)
        val addEmployeeButton = findViewById<Button>(R.id.addEmployeeButton)

        addEmployeeButton.setOnClickListener {
            try {
                val name = employeeNameEditText.text.toString()
                val age: Int = validator.validateIntValue(employeeAgeEditText.text.toString())
                val parentId: Int? =
                    validator.validateParentId(employeeParentIdEditText.text.toString())

                employeeRepository.addEmployee(name, age, Address(), emptyList(), parentId)

                showToast("Employee $name successfully added!")

                employeeNameEditText.text.clear()
                employeeParentIdEditText.text.clear()
                employeeAgeEditText.text.clear()

            } catch (ex: InputDataValidator.ValidatorException) {
                showError(ex.localizedMessage ?: "")
            } catch (ex: RepositoryException) {
                showError(ex.localizedMessage ?: "")
            }

            hideKeyboard(this.rootLayout)
        }
    }

    private fun getEmployeeBlock() {
        val getEmployeeNameEditText = findViewById<EditText>(R.id.getEmployeeNameEditText)
        val getEmployeeButton = findViewById<Button>(R.id.getEmployeeButton)
        val employeeInfoTextView = findViewById<TextView>(R.id.employeeInfoTextView)

        getEmployeeButton.setOnClickListener {
            employeeInfoTextView.text = ""
            val name = getEmployeeNameEditText.text.toString()

            try {
                val employee = employeeRepository.getEmployeeByName(name)
                if (employee == null) {
                    showError("Employee with name \" $name \" not found")
                } else {
                    employeeInfoTextView.text = employee.toString()
                }
            } catch (ex: RepositoryException) {
                showError(ex.localizedMessage ?: "")
            }

            hideKeyboard(this.rootLayout)
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun showError(message: String) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show()
    }
}