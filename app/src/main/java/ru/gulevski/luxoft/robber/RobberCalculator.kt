package ru.gulevski.luxoft.robber

import kotlin.math.max

internal class RobberCalculator {
    /**
     * Returns maximum sum of non-adjacent numbers in array
     * Assumptions:
     * 1. Sum of money in houses must be less or equal than Int.MAX_VALUE (2 147 483 647)
     * 2. Count of houses must be less or equal than Int.MAX_VALUE (2 147 483 647)
     * @param moneyArray - array of non-negative integers
     * @return max sum
     */
    internal fun getMaxAmountOfMoney(moneyArray: IntArray): Int {
        if (moneyArray.isEmpty()) return 0
        if (moneyArray.size == 1) return moneyArray[0]

        var sumIncludedCur = moneyArray[0]
        var sumExcludedCur = 0
        var tmp: Int

        for (i in 1 until moneyArray.size) {
            tmp = max(sumIncludedCur, sumExcludedCur)
            sumIncludedCur = sumExcludedCur + moneyArray[i]
            sumExcludedCur = tmp
        }

        return max(sumIncludedCur, sumExcludedCur)
    }
}