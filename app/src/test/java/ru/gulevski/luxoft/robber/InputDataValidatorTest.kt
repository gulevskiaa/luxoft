package ru.gulevski.luxoft.robber

import org.junit.Test

import org.junit.Assert.*
import ru.gulevski.luxoft.main.util.InputDataValidator

class InputDataValidatorTest {

    private val validator = InputDataValidator()

    @Test(expected = InputDataValidator.ValidatorException::class)
    fun `validate data when string is empty`() {
        val intArray = validator.validateIntArray("")
    }

    @Test(expected = InputDataValidator.ValidatorException::class)
    fun `validate data when string is incorrect`() {
        val intArray = validator.validateIntArray(" , a, b, d , 2")
    }

    @Test(expected = InputDataValidator.ValidatorException::class)
    fun `validate data when numbers separated by other symbol`() {
        val intArray = validator.validateIntArray("1;2")
    }

    @Test
    fun `validate data when numbers separated by comma with blanks`() {
        val intArray = validator.validateIntArray("1 ,  2, 3 ,5")
        assertArrayEquals(intArrayOf(1, 2, 3, 5), intArray)
    }

    @Test
    fun `validate data when numbers separated by comma with large value`() {
        val intArray = validator.validateIntArray("1,2,3,2 147 483 647")
        assertArrayEquals(intArrayOf(1, 2, 3, Int.MAX_VALUE), intArray)
    }

    @Test(expected = InputDataValidator.ValidatorException::class)
    fun `validate data when numbers separated by comma with value bigger than MAX_VALUE`() {
        val intArray = validator.validateIntArray("1,2,3,2 147 483 648")
    }
}