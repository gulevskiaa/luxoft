package ru.gulevski.luxoft.robber

import org.junit.Assert.assertEquals
import org.junit.Test

internal class RobberCalculatorTest {

    private val calculator = RobberCalculator()

    @Test
    fun `get sum when array is empty`() {
        val maxSum = calculator.getMaxAmountOfMoney(intArrayOf())
        assertEquals(0, maxSum)
    }

    @Test
    fun `get sum when array has 1 element`() {
        val maxSum = calculator.getMaxAmountOfMoney(intArrayOf(1))
        assertEquals(1, maxSum)
    }

    @Test
    fun `get sum when array has 2 elements`() {
        val maxSum = calculator.getMaxAmountOfMoney(intArrayOf(5, 1))
        assertEquals(5, maxSum)
    }

    @Test
    fun `get sum when array has 3 elements`() {
        val maxSum = calculator.getMaxAmountOfMoney(intArrayOf(5, 8, 2))
        assertEquals(8, maxSum)
    }

    @Test
    fun `get sum when array has 4 elements`() {
        val maxSum = calculator.getMaxAmountOfMoney(intArrayOf(10, 12, 3, 22))
        assertEquals(34, maxSum)
    }

    @Test
    fun `get sum when array has 10 elements`() {
        val maxSum = calculator.getMaxAmountOfMoney(intArrayOf(10, 12, 3, 13, 4, 20, 21, 1, 2, 3))
        // 12 + 13 + 21 + 3 = 49
        assertEquals(49, maxSum)
    }
}