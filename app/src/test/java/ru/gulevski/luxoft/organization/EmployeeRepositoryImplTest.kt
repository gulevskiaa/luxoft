package ru.gulevski.luxoft.organization

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import ru.gulevski.luxoft.organization.model.Address
import ru.gulevski.luxoft.organization.model.Employee
import ru.gulevski.luxoft.organization.exception.RepositoryException
import ru.gulevski.luxoft.organization.repository.EmployeeRepository
import ru.gulevski.luxoft.organization.repository.EmployeeRepositoryImpl

class EmployeeRepositoryImplTest {

    private val repository: EmployeeRepository = EmployeeRepositoryImpl()

    @Before
    fun setUp() {
        repository.removeAll()
    }

    @Test
    fun `adding and getting boss`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        assertEquals("Employee(id=1, name=John)", repository.getEmployeeByName("John").toString())
    }

    @Test(expected = RepositoryException::class)
    fun `adding first employee with non-null parent`() {
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
    }

    @Test(expected = RepositoryException::class)
    fun `adding second employee with null parent`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), null)
    }

    @Test(expected = RepositoryException::class)
    fun `adding employee with non-existent parent`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 2)
    }

    @Test
    fun `adding employee with existent parent`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
        repository.addEmployee("Ann", 35, Address(), listOf(), 2)
    }

    @Test
    fun `getting employee with non-existent name`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
        repository.addEmployee("Ann", 35, Address(), listOf(), 2)
        assertEquals(null, repository.getEmployeeByName("Andrew"))
    }

    @Test
    fun `getting employee with existent name`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
        repository.addEmployee("Ann", 35, Address(), listOf(), 2)
        assertEquals("Ann", repository.getEmployeeByName("Ann")?.name)
    }

    @Test
    fun `changing employee age`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
        val mike = repository.getEmployeeByName("Mike")
        mike?.age = 63
        assertEquals(63, repository.getEmployeeByName("Mike")?.age)
    }

    @Test
    fun `get id for the first record`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
        repository.addEmployee("Alice", 38, Address(), listOf(), 1)
        assertEquals(1, repository.getEmployeeByName("John")?.id)
    }

    @Test
    fun `get id for record`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
        repository.addEmployee("Alice", 38, Address(), listOf(), 1)
        assertEquals(2, repository.getEmployeeByName("Mike")?.id)
    }

    @Test
    fun `get id for the last record`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
        repository.addEmployee("Alice", 38, Address(), listOf(), 1)
        repository.addEmployee("Mark", 28, Address(), listOf(), 2)
        repository.addEmployee("Samantha", 46, Address(), listOf(), 4)
        assertEquals(5, repository.getEmployeeByName("Samantha")?.id)
    }

    @Test
    fun `get parentId for the last record`() {
        repository.addEmployee("John", 57, Address(), listOf(), null)
        repository.addEmployee("Mike", 57, Address(), listOf(), 1)
        repository.addEmployee("Alice", 38, Address(), listOf(), 1)
        repository.addEmployee("Mark", 28, Address(), listOf(), 2)
        repository.addEmployee("Samantha", 46, Address(), listOf(), 3)
        assertEquals(3, repository.getEmployeeByName("Samantha")?.parentId)
    }
}